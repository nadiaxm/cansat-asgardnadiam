/*
  Program to send a SOS (Version 1)
*/

constexpr unsigned int DotDuration = 200; // msec
constexpr unsigned int DashDuration = 600; // msec
constexpr unsigned int InterSignDelay = 200; // msec.
constexpr unsigned long SOS_Period = 8000; // msec
constexpr byte ledSOS = 13;

enum MorseSign_t {
  dot,
  dash
};

void setup()
{
  Serial.begin(19200);
  while (!Serial) ;
  pinMode(ledSOS, OUTPUT);
}

//------------------------ Morse sign logic ---------------------------

enum MorseSignState_t {
  On,
  Off,
  SignDone
};

byte signLedNumber;
byte signNumRepetitions;
MorseSign_t signType;
MorseSignState_t signState;
byte signCounter;
unsigned long signTimestamp;

void startMorseSign(byte ledNbr, MorseSign_t sign, byte numRepetitions ) {
  signLedNumber = ledNbr;
  signNumRepetitions = numRepetitions;
  signType = sign;
  signCounter=0;
  signState = Off;
  Serial.println("Starting sign");
}

bool runMorseSign() {
  switch (signState)
  {
    case On:
      if ( ((millis() - signTimestamp) >= DashDuration)
           || ((signType == dot) && ((millis() - signTimestamp) >= DotDuration)) ) {
        digitalWrite(signLedNumber, LOW);
        signTimestamp = millis();
        signState = Off;
        signCounter++;
      }
      break;
    case Off:
      if (signCounter == signNumRepetitions) {
        signState = SignDone;
      } else {
        if ((millis() - signTimestamp) >= InterSignDelay) {
          digitalWrite(signLedNumber, HIGH);
          signTimestamp = millis();
          signState = On;
        }
      }
      break;
    case SignDone:
      break;
    default:
      Serial.println("Unexpected sign state");
  }
  return (signState==SignDone);
}

//------------------------ SOS logic ------------------------------------
enum SOS_Stage {
  Start,
  FirstChar,
  SecondChar,
  ThirdChar,
  Done
};

SOS_Stage sosCurrentState = Done;

void startSOS() {
  sosCurrentState = Start;
}

bool runSOS() {
  bool charDone;
  switch (sosCurrentState) {
    case Start:
      startMorseSign(ledSOS, dot, 3);
      sosCurrentState = FirstChar;
      break;
    case FirstChar:
      charDone = runMorseSign();
      if (charDone) {
        startMorseSign(ledSOS, dash, 3);
        sosCurrentState = SecondChar;
      }
      break;
    case SecondChar:
      charDone = runMorseSign();
      if (charDone) {
        startMorseSign(ledSOS, dot, 3);
        sosCurrentState = ThirdChar;
      }
      break;
    case ThirdChar:
      charDone = runMorseSign();
      if (charDone) {
        sosCurrentState = Done;
      }
      break;
    case Done:
      break;
    default:
      Serial.println("Error: unexpected sos state");
  }
  return (sosCurrentState == Done);
}

//--------------------------- Loop ----------------------------------
unsigned long lastSOS_At = 0L;

void loop()
{
  bool done = runSOS();
  if (done) {
    if ((millis() - lastSOS_At) >= SOS_Period) {
      Serial.println("SOS started");
      startSOS();
      lastSOS_At = millis();
    }
  }
}
