/*
   ClockSynchronizer_Master.cpp
*/
#include "ClockSynchronizer.h"
#include "Wire.h"

#define DEBUG
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_INIT 0

bool ClockSynchronizer_Master::setMasterClock(byte slaveAddress, byte maxDurationInSec) {
  byte result = 100;

  DPRINTS(DBG_INIT, "Sync. with slave #");
  DPRINT(DBG_INIT, slaveAddress);
  unsigned long endTS = millis() + maxDurationInSec * 1000;
  unsigned long ts;
  while (result != 0) {
    DPRINTS(DBG_INIT, ".");
    Wire.beginTransmission(slaveAddress);
    ts = millis();
    Wire.write((uint8_t*) &ts, sizeof(ts));
    result = Wire.endTransmission();
    // Do not retry if success, duration==0 or endTS exceeded
    if (result==0 || (maxDurationInSec==0) || (endTS < millis())) {
  		  break;
    }
    delay(10);
  }
  if (result == 0) {
    DPRINTSLN(DBG_INIT, " OK");
    DPRINTS(DBG_INIT, "Transmitted ");
    DPRINTLN(DBG_INIT, ts);
    return true;
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, " Timeout!");
    return false;
  }
}
