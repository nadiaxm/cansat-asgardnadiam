
 #define USE_ASSERTIONS
 #include "DebugCSPU.h"
 #include "TestHardwareScanner.h"
 #include "Arduino.h"

byte TestHardwareScanner::getNumExternalEEPROM() const {
   Serial << F("Forcing 0 EEPROM detection") << ENDL;
   return 0;
 }

